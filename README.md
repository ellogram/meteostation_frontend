# meteo_station_cmm_vue

## Présentation du sujet

Nous avions pour objectif de réaliser la partie logicielle d'un centrale (frontend) et une sonde (backend), toutes les deux appartenant à une station météo.
Cette station météo est composée de 3 centrales et de trois sondes comme sur le schéma qui suit :
![alt text](images/sondeCentraleMeteo.jpg)
Architecture de la station météo

## Travail demandé

Déveloper une API avec les fonctionnalités suivantes :

- Récupération de la dernière valeur d'une donnée d'un capteur ou de l'ensemble des capteurs,
- Récupération d'un échantillon de données (un ou plusieurs capteurs) sur une période donnée.


## Technologies utilisées

La partie frontend a été développée avec les technologies suivantes :

- NodeJs + Vue.js,
- Composant Vue Highcharts ,
- Composant Vue Material Design Icon,
- Composant Vue grid layout.

La partie backend a été développée avec les technologies suivantes :

- NodeJs,
- InfluxDb pour la gestion de la base de données.

## La sonde (backend)

Les sondes utilisées ici, sont des RaspberryPi.
Identification de notre matériel (durant le développement du projet) :
- Notre sonde est identifiée par le nom piensg009 sur le réseau l'ENSG
- Le port de communincation choisi 3001

*port 3001 choisi par les tous groupes*

### logiciel backend

Le logiciel de la sonde peut être récupéré sur le git suivant :

```url
https://gitlab.com/max.permalnaick/piservernode
```

### installation du logiciel backend

Pour installer le logiciel sur une machine, il faut utiliser **npm** (à installer si nécessaire).

*version npm pour le développement v6.6.0*

**installation de influxdb**

```bash
sudo apt install influxdb
sudo apt install influxdb-client
```

**installation du logiciel**

```bash
git clone 'https://gitlab.com/max.permalnaick/piservernode.git'
cd piservernode
npm install
npm start
```

**Le logiciel est déjà présent sur la sonde.**
**Un service le lance automatiquement lors du démarrage.**

### API développé pour le backend

#### Récupération de la dernière valeur d'un capteur (Température, Hygrométrie, Pression atmosphérique, Pluviométrie, Luminosité, Vitesse et direction du vent, Position GPS et heure)

Format de la trame pour un capteur :

```
http://piensg009:3001/last?capteur_type=typeDeCapteur
```

#### Retour 1 type

Le résultat est sous forme de json :

```
{"id":"009","name":"SONDE_MCM","measurements":{"date":"2019-01-21T12:25:44.997Z","temp":-89.182}}
```

**capteur_type** : variable type de capeur

Voici la liste des mots clés :
date, temp, hygro, press, lum, wind_dir, wind_mean, wind_min, wind_max, position, rain

#### Récupération de la dernière valeur de l'ensemble des capteurs

Format de la trame pour l'ensemble des capteurs

```
http://piensg009:3001/last?capteur_type=all
```

#### Retour all

Le résultat est sous forme de json :

```
{"id":"009","name":"SONDE_MCM","location":{"lat":"48.11696666666667","lng":"-11.5217","date":"2019-01-31T22:36:44.000Z"},"measurements":{"date":"2019-01-31T22:36:44.659Z","press":977.715,"temp":-89.252,"hygro":13.59,"lum":1,"wind_min":25.8,"wind_max":60.7,"wind_mean":40.4,"wind_dir":177.589746260625},"rain":"2019-01-31T22:36:42.150Z"}
```

#### Echec ou erreur

Pour une valeur de **capteur_type** n'appartenant pas à la liste :

```
{"error":"variable non définie : undefined"}
```

Pour une variable non définie : 

```
{"error":"variable non définie!"}
```

#### Exemples

```
{
    "id": "009",
    "name": "SONDE_MCM",
    "measurements": {
        "date": "2019-01-31T17:32:04.561Z",
        "temp": -31.431,
        "hygro": 22.826,
        "press": 978.383,
        "lum": 8571,
        "wind_dir": 181.636050594649,
        "wind_mean": 38.2,
        "wind_min": 67,
        "wind_max": 23.9
    },
    "location": {
        "lat": 49.45398333333333,
        "lng": -3.294766666666667,
        "date": "2019-01-31T17:32:04.560Z"
    },
    "rain": "2019-01-31T17:31:45.696Z"
}
```

Pour un capteur en particulier (exemple: température)
```
{
    "id": "009",
    "name": "SONDE_MCM",
    "measurements": {
        "date": "2019-01-31T17:32:34.666Z",
        "temp": -31.552
    }
}
```

Pour le GPS:
```
{
    "id": "009",
    "name": "SONDE_MCM",
    "location": {
        "lat": 49.45458333333333,
        "lng": -3.294766666666667,
        "date": "2019-01-31T17:33:04.770Z"
    }
}
```

Pour la pluviométrie:
```
{
    "id": "009",
    "name": "SONDE_MCM",
    "rain": "2019-01-31T17:33:25.857Z"
}
```

#### Récupération d'un ensemble de valeurs de capteurs sur un période (Température, Hygrométrie, Pression atmosphérique, Pluviométrie, Luminosité, Vitesse et direction du vent, Position GPS et heure)

Format de la trame pour l'ensemble des capteurs

```
http://piensg009:3001/period?capteur_type=all&dateStart=XXXXXX&dateEnd=YYYYYY
```


**capteur_type** : variable type de capteur
**dateStart** : date de type unix timestamp
**dateEnd** : date de type unix timestamp


#### Retour all

Le résultat est sous forme de json :

```
id	"009"
name	"SONDE_MCM"
dataLength	2506
data	[…]
rainLength	100
rain	[]
```

#### Echec ou erreur

Pour une valeur de **capteur_type** n'appartenant pas à la liste :

```
{"error":"variable non définie : undefined"}
```

Pour une variable non définie : 

```
{"error":"variable non définie!"}
```

