module.exports = {
  devServer: {
    proxy: {
      "/ip-api": {
        target: "http://ip-api.com/json/",
        secure: false,
        pathRewrite: {
          '^/ip-api': '', // rewrite path
        },
      },
      "/meteo-api": {
        target: "https://api.meteostat.net/v1/",
        secure: false,
        pathRewrite: {
          '^/meteo-api': '', // rewrite path
        },
      }
    }
  }
};
